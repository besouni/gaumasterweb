<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>PHP 1</title>
</head>
<body>
   <h1>Hello PHP</h1>
   <?php
      echo "Hello World!!!";
      $x = 10;
      $y = 34.8;
      $z = "PHP";
      $m = [3, 9.8, "Hello Wold", true];
      echo "<br>".$x;
      $assm = ["name"=>["vano", "vako"], "age"=>45];
      echo "<pre>";
      print_r($m);
      echo "</pre>";
      echo "<pre>";
      print_r($assm);
      echo "</pre>";
   ?>
</body>
</html>