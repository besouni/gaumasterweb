<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Example Get, Post</title>
   <link rel="stylesheet" href="style.css">
</head>
<body>
   <div>
      <ul>
         <li><a href="example.php">example.php</a></li>
         <li><a href="example.php?cat=html">HTML</a></li>
         <li><a href="example.php?cat=css">CSS</a></li>
      </ul>
   </div>
   <section>
      <?php
         include "files/route.php";
      ?>
   </section>
</body>
</html>