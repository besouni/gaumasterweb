<?php
   include "connection.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>MySQL</title>
   <style>
      .container{
         border: solid 1px black;
         min-height: 100vh;
         width: 50%;
         margin: auto;
      }

      table {
         width: 100%;
         border-collapse: collapse;
      }

      table td{
         border-bottom: 1px solid black;
         padding: 10px;
      }
   </style>
</head>
<body>
   <div class="container">
      <table>
         <tbody>
         <?php
            $query = "SELECT * FROM users";
            $result = mysqli_query($connection,  $query);
            var_dump($result);
            echo "<hr>";
            echo mysqli_num_rows($result);
            if(mysqli_num_rows($result)>0){
               // $row = mysqli_fetch_all($result);
               // $row = mysqli_fetch_assoc($result);
               // echo "<pre>";
               // print_r($row);
               // echo "</pre>";
               while($row = mysqli_fetch_assoc($result)){
         ?>
            <tr>
               <td><?=$row['id']?></td>
               <td><?=$row['name']?></td>
               <td><?=$row['lastname']?></td>
               <td><?=$row['address']?></td>
            </tr>
      <?php }}?>
         </tbody>
      </table>
   </div>
</body>
</html>