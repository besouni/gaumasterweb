<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Page 1</title>
    <style>
        body{
            padding-left: 200px;
        }
    </style>
</head>
<body>
    <h1>Page1</h1>
    <a href="page2.php">Page 2</a>
    <br><br>
    <a href="page3.php">Page 3</a>
    <hr>
    <?php
        $x1 = 98;
        echo "x1 = ".$x1;
        echo "<br>";
        setcookie("x2", 37, time()+20, "/");
        setcookie("x3", 38, time()+20*365*24*3600, "/");
        setcookie("x4", 39, time()+20*365*24*3600, "/");
        echo "<hr>";
        echo time();
        echo "<hr>";
        echo  $_COOKIE["x2"];
        echo "<br>";
        echo  $_COOKIE["x3"];
        echo "<br>";
        echo  $_COOKIE["x4"];
    ?>
</body>
</html>