<?php
    session_start();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Page 1</title>
    <style>
        body{
            padding-left: 200px;
        }
    </style>
</head>
<body>
    <h1>Page1</h1>
    <a href="page2.php">Page 2</a>
    <br><br>
    <a href="page3.php">Page 3</a>
    <br><br>
    <a href="page4.php">Page 4</a>
    <hr>
    <?php
        $x1 = 98;
        echo "x1 = ".$x1;
        echo "<br>";
        $_SESSION["x2"] = 99;
        $_SESSION["x3"] = 100;
        $_SESSION["x4"] = 102;
        echo "Session x2 = ".$_SESSION["x2"];
        echo "<br>";
        echo "Session x3 = ".$_SESSION["x3"];
        echo "<br>";
        echo "Session x4 = ".$_SESSION["x4"];
        echo "<br>";
    ?>
</body>
</html>