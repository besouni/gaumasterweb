<?php
    session_start();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Page 4</title>
    <style>
        body{
            padding-left: 200px;
        }
    </style>
</head>
<body>
    <h1>Page4</h1>
    <a href="page2.php">Page 2</a>
    <br><br>
    <a href="page3.php">Page 3</a>
    <br><br>
    <a href="page1.php">Page 1</a>
    <hr>
    <?php
        echo "x1 = ".$x1;
        echo "<br>";
        echo "Session x2 = ".$_SESSION["x2"];
        echo "<br>";
        echo "Session x3 = ".$_SESSION["x3"];
        echo "<br>";
        echo "Session x4 = ".$_SESSION["x4"];
        echo "<br>";
        print_r($_SESSION);
    ?>
</body>
</html>