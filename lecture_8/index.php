<?php
   include "blocks/connection.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>MySQL</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="main">
        <div class="nav">
            <ul>
                <li><a href="index.php">HOME</a></li>
                <li><a href="?menu=select">SELECT</a></li>
                <li><a href="?menu=insert">INSERT</a></li>
            </ul>
        </div>
        <div class="container">
            <?php
                if(isset($_GET['menu'])&&$_GET['menu']=='select'){
                    include "blocks/select.php";
                }else if(isset($_GET['menu'])&&$_GET['menu']=='insert'){
                    include "blocks/insert.php";
                }else if(isset($_GET['menu'])&&$_GET['menu']=='delete'){
                    include "blocks/delete.php";
                }else if(isset($_GET['menu'])&&$_GET['menu']=='edit'){
                    include "blocks/edit.php";
                }
            ?>
        </div>
    </div>
</body>
</html>