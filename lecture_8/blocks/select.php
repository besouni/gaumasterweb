<table>
    <thead>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Lastname</th>
        <th>Birthday</th>
        <th>Salary</th>
        <th>Address</th>
        <th>Edit</th>
        <th>Delete</th>
    </tr>
    </thead>
    <tbody>
        <?php
        $query = "SELECT * FROM users ORDER BY id DESC";
        $result = mysqli_query($connection,  $query);
        if(mysqli_num_rows($result)>0){
            while($row = mysqli_fetch_assoc($result)){
                ?>
                <tr>
                    <td><?=$row['id']?></td>
                    <td><?=$row['name']?></td>
                    <td><?=$row['lastname']?></td>
                    <td><?=$row['birthday']?></td>
                    <td><?=$row['salary']?></td>
                    <td><?=$row['address']?></td>
                    <td><a href="?menu=edit&&id=<?=$row['id']?>">EDIT</a></td>
                    <td><a href="?menu=delete&&id=<?=$row['id']?>">DELETE</a></td>
                </tr>
            <?php }}?>
    </tbody>
</table>