<table>
    <thead>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Lastname</th>
        <th>Birthday</th>
        <th>Salary</th>
        <th>Address</th>
    </tr>
    </thead>
    <tbody>
        <?php
        $query = "SELECT * FROM users ORDER BY id DESC";
        $result = mysqli_query($connection,  $query);
        if(mysqli_num_rows($result)>0){
            while($row = mysqli_fetch_assoc($result)){
                ?>
                <tr>
                    <td><?=$row['id']?></td>
                    <td><?=$row['name']?></td>
                    <td><?=$row['lastname']?></td>
                    <td><?=$row['birthday']?></td>
                    <td><?=$row['salary']?></td>
                    <td><?=$row['address']?></td>
                </tr>
            <?php }}?>
    </tbody>
</table>