<?php
    session_start();
   include "blocks/connection.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>MySQL</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="main">
        <div class="nav">
            <ul>
                <li><a href="index.php">HOME</a></li>
                <?php
                    if(!isset($_SESSION['email'])){
                ?>
                <li style="margin-top: 50px"><a href="?menu=signup">Sign Up</a></li>
                <li><a href="?menu=signin">Sign In</a></li>
                <?php
                    }else{
                 ?>
                <li><a href="?menu=select">ACTION</a></li>
                <li><a href="?menu=insert">INSERT</a></li>
                <li style="margin-top: 50px"><a href="?menu=signout">Sign Out</a></li>
                <?php
                    }
                ?>
            </ul>
        </div>
        <div class="container">
            <?php
                if(isset($_GET['menu'])&&$_GET['menu']=='select')
                {
                    include "blocks/select.php";
                }
                else if(isset($_GET['menu'])&&$_GET['menu']=='insert')
                {
                    include "blocks/insert.php";
                }
                else if(isset($_GET['menu'])&&$_GET['menu']=='delete')
                {
                    include "blocks/delete.php";
                }
                else if(isset($_GET['menu'])&&$_GET['menu']=='edit'){
                    include "blocks/edit.php";
                }
                else if(isset($_GET['menu'])&&$_GET['menu']=='signup'){
                    include "blocks/signup.php";
                }
                else if(isset($_GET['menu'])&&$_GET['menu']=='signin'){
                    include "blocks/signin.php";
                }
                else if(isset($_GET['menu'])&&$_GET['menu']=='signout'){
                    unset($_SESSION['email']);
                    header("location: index.php");
                }
                else{
                    include "blocks/home.php";
                }
            ?>
        </div>
    </div>
</body>
</html>